from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^$', views.register_view, name='register_view_url'),
    url(r'^profile/(?P<pk>\d+)/$', views.detail_view, name='profile_view_url'),
]
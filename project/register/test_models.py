from django.test import TestCase
from django.core.urlresolvers import reverse
from models import DetailedUser
from django.contrib.auth.models import User


class ModelsTest(TestCase):
    """
    Test for all models in the app
    """

    def test_create_user(self):
        user = User.objects.create_user('cuong', 'cuong@gmail.com', '1234abc', first_name='haha')
        new_detail_user = DetailedUser(user=user, gender='m', nickname='cool', date_of_birth='1994-12-25')
        new_detail_user.save()
        self.assertEqual(new_detail_user.user.first_name, 'haha')
        self.assertEqual(user.detaileduser.gender, 'm')


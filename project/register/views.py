from django.shortcuts import render
from . import forms
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User
from django.views import generic
from django.core.urlresolvers import reverse
from django.contrib.auth import authenticate, login


def register_view(request):
    user_status = ''
    if request.method == 'POST':
        form = forms.RegisterForm(request.POST)
        if form.is_valid():
            User.objects.create_user(username=request.POST['username'],
                                     email=request.POST['email'],
                                     password=request.POST['password'])
            auth_user = authenticate(username=request.POST['username'],
                                     password=request.POST['password'])
            if auth_user is not None:
                if auth_user.is_active:
                    login(request, auth_user)
                    return HttpResponseRedirect(reverse('register:profile_view_url',
                                                        kwargs={"pk": auth_user.pk}))
                else:
                    return HttpResponseRedirect('/register/', {user_status: 'inactive'})
            else:
                return HttpResponseRedirect('/register/', {user_status: 'unauth'})
    else:
        form = forms.RegisterForm()
    return render(request, 'register/register.html', {'form': form})


def detail_view(request, pk):
    if request.user.is_authenticated() and request.user.pk == int(pk):
        return render(request, 'register/profile.html', {'user': request.user,
                                                         'user_status': 'success'})
    else:
        return HttpResponseRedirect('/register/')



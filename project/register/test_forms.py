from django.test import TestCase
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User


class FormsTest(TestCase):
    """
    Test for all forms in the app
    """

    def test_display_form(self):
        response = self.client.get('/register/')
        self.assertContains(response, 'Username')
        self.assertContains(response, 'Password')
        self.assertContains(response, 'Email')
        self.assertNotContains(response, 'Welcome')
        self.assertNotContains(response, 'This account is inactive')
        self.assertNotContains(response, 'Please register')

    def test_register_user(self):
        self.client.post('/register/',
                         {'username': 'cuong', 'password': '123',
                          'email': 'cuong@jamja.vn'})
        new_user = User.objects.get(pk=1)
        self.assertIsNotNone(new_user)
        self.assertEqual(new_user.username, 'cuong')
        self.assertEqual(new_user.email, 'cuong@jamja.vn')

    def test_redirect_succesful_after_register(self):
        response = self.client.post('/register/',
                                    {'username': 'cuong', 'password': '123',
                                     'email': 'cuong@jamja.vn'}, follow=True)
        self.assertContains(response, 'Username')
        self.assertContains(response, 'cuong')
        self.assertContains(response, 'Email')
        self.assertContains(response, 'cuong@jamja.vn')
        self.assertContains(response, 'Logout')
        self.assertContains(response, 'Welcome')
        self.assertNotContains(response, 'This account is inactive')
        self.assertNotContains(response, 'Please register')

    def test_redirect_inactive_user(self):
        response = self.client.post('/register/',
                                    {'username': 'cuong', 'password': '123',
                                     'email': 'cuong@jamja.vn'}, follow=True)
        new_user = User.objects.get(pk=1)
        new_user.is_active = False
        new_user.save()


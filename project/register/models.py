from django.db import models
from django.contrib.auth.models import User


class DetailedUser(models.Model):
    choices = (
        ('m', 'male'),
        ('f', 'female'),
        ('o', 'other')
    )
    user = models.OneToOneField(User)
    gender = models.CharField(max_length=1, choices=choices, null=True)
    nickname = models.CharField(max_length=20, null=True)
    date_of_birth = models.DateField(null=True)

    def __unicode__(self):
        return self.user.first_name + self.user.last_name
